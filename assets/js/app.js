// Dark Theme - Mobile Menu
const bodyPage = document.querySelector('body');
const btnTema = document.getElementById('btnTema');
const btnTema2 = document.getElementById('btnTema2');
const menuMobile = document.getElementById('btnMenu');
const menuMobileClose = document.getElementById('btnClose');
const panelMobile = document.getElementById('panelMobile');

// Modals

const linkTerminos = document.getElementById('linkTerminos');
const linkPoliticas = document.getElementById('linkPoliticas');

const modalTerminos = document.getElementById('modalTerminos');
const modalPoliticas = document.getElementById('modalPoliticas');

const closeModalTerminos = document.getElementById('closeModalTerminos');
const closeModalPoliticas = document.getElementById('closeModalPoliticas');


const cambiaTema = () => {
    bodyPage.classList.toggle('theme__dark');
}

// Change Theme
btnTema.addEventListener('click', cambiaTema);
btnTema2.addEventListener('click', cambiaTema);

// Open Menu Mobile
menuMobile.addEventListener('click', () => {
  panelMobile.classList.toggle('open');
  bodyPage.style.overflow = "hidden";
})

// Close Menu Mobile
menuMobileClose.addEventListener('click', () => {
  panelMobile.classList.toggle('open');
  bodyPage.style.overflow = "auto";
})



// Typewriter

const texts = ['entrega gobernanza', 'empodera tus finanzas', 'genera valor']
let count = 0;
let index = 0;
let currentText = '';
let letter = '';

(function type() {
 if( count === texts.length ) {
   count = 0;
 }
currentText = texts[count];
letter = currentText.slice(0, ++index);

document.querySelector('.typeText').textContent = letter;
if( letter.length === currentText.length) {
  count++;
  index = 0;
  setTimeout(type, 4000);
} else {
  setTimeout(type, 150)
}

}());


// Modals

linkPoliticas.addEventListener('click', () => {
  modalPoliticas.classList.add('show-modal');
  bodyPage.style.overflow = "hidden";
})





closeModalPoliticas.addEventListener('click', () => {
  modalPoliticas.classList.remove('show-modal');
  bodyPage.style.overflow = "auto";
})


// Hide modal on outside click

window.addEventListener('click', e => e.target == modalPoliticas ? modalPoliticas.classList.remove('show-modal') : false);